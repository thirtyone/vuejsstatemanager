const state = {
    delivery_details : {},
    payment_method : '',
    done_steps : {
        sign_in : false,
        delivery_details : false,
        payment : false
    }
};
const getters = {
    getDeliveryDetails : (state) => state.delivery_details,
    getPaymentMethod : (state) => state.payment_method,
    getDoneSteps : (state) => state.done_steps,
};

const actions = {
    async setDeliveryDetails({ commit },details) {
        commit('setDeliveryDetails',details)
    },
    async setPaymentMethod({ commit },details) {
        commit('setPaymentMethod',details)
    },
    async setSignInAsDone({ commit },bool) {
        commit('setSignInAsDone',bool);
    },
    async setDeliveryDetailsAsDone({ commit },bool) {
        commit('setDeliveryDetailsAsDone',bool);
    },
    async setPaymentMethodAsDone({ commit },bool) {
        commit('setPaymentMethodAsDone',bool);
    },
};

const mutations = {
    setDeliveryDetails : (state,details) => {
        state.delivery_details = details;
        state.done_steps.delivery_details = true;
    },
    setPaymentMethod : (state,payment_method) => {
        state.payment_method = payment_method;
        state.done_steps.payment = true;
    },
    setSignInAsDone : (state,bool = true) => {
        state.done_steps.sign_in = bool;
        setTimeout(() => {
            document.getElementById('delivery').click();
        },100);
    },
    setDeliveryDetailsAsDone : (state,bool = true) => {
        state.done_steps.delivery_details = bool;
        setTimeout(() => {
            document.getElementById('payment').click();
        },100);
    },
    setPaymentMethodAsDone : (state,bool = true) => {
        state.done_steps.payment = bool;
        setTimeout(() => {
            document.getElementById('place-order').click();
        },100);
    },

};


export default {
    state,
    getters,
    actions,
    mutations
}