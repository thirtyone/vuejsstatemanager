let state = {
    items : [
        {
            id : 1,
            name : "test product",
            size : {id : 1, name : 'small'},
            ime_src:"http://bizu.thirtyonedigital.co/uploads/images/1571398439Mediterranean Salad 1.jpg",
            price : 400.00,
            quantity : 1
        },
        {
            id : 2,
            name : "test product",
            size : {id : 2, name : 'medium'},
            ime_src:"http://bizu.thirtyonedigital.co/uploads/images/1571398439Mediterranean Salad 1.jpg",
            price : 200.00,
            quantity : 1
        }
    ],
    subTotal : 600.00,
    total : 600.00,
    discount : 40.00,
    total_items : 2
};

if(localStorage.getItem('cart') !== null && localStorage.getItem('cart') !== undefined )
    state = JSON.parse(localStorage.getItem('cart'));

const getters = {
    getCartItems : (state) => state.items,
    getCartTotalItems : (state) => state.total_items,
    getDiscount : (state) => state.discount,
    getTotal : (state) => state.total,
    getSubTotal : (state) => state.subTotal,
};

const actions = {
    async addCartItem({ commit },item) {
        commit('addItem',item)
    }
};

const mutations = {
    addItem : (state,item) => {
        let item_exist = state.items.some(el => {
            if(el.id == item.id){
                el.quantity = (parseInt(el.quantity) + parseInt(item.quantity));
                return true;
            }
            return false;
        });
        if(false === item_exist){
            state.items.unshift(item);
            state.total_items += 1;
        }

        state.subTotal = ( parseInt(state.subTotal) + ( parseInt(item.price) * parseInt(item.quantity)));
        state.total = parseInt(state.subTotal) - parseInt(state.discount);

        localStorage.setItem('cart', JSON.stringify(state));
    }
};


export default {
    state,
    getters,
    actions,
    mutations
}