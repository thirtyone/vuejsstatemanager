const state = {
    info : {},
    order_history : {}
};
const getters = {
    getInfo : (state) => state.info,
    getOrderHistory : (state) => state.order_history,
};

const actions = {
    async register({ commit },details) {
        commit('register',details)
    },
};

const mutations = {
    register : (state,info) => {
        state.info = info;
    },
};


export default {
    state,
    getters,
    actions,
    mutations
}