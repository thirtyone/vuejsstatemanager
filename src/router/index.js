import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home
  },
  {
    path: '/about',
    name: 'about',
    component: () => import('../views/About.vue')
  },
  {
    path: '/order/:slug',
    name: 'order',
    component: () => import('../views/OrderProduct.vue')
  },
  {
    path: '/cart',
    name: 'cart',
    component: () => import('../views/UserCart.vue')
  },
  {
    path: '/products',
    name: 'products',
    component: () => import('../views/Products.vue')
  },
  {
    path: '/contact',
    name: 'contact',
    component: () => import('../views/Contact.vue')
  },
  {
    path: '/checkout',
    name: 'checkout',
    component: () => import('../views/Checkout.vue')
  },
  {
    path: '/my-profile',
    name: 'myProfile',
    component: () => import('../views/MyProfile.vue')
  },
  {
    path: '/order-history',
    name: 'orderHistory',
    component: () => import('../views/OrderHistory.vue')
  },
]

const router = new VueRouter({
  routes,
  scrollBehavior() {
      return {x: 0, y: 0}
  }
})

export default router
