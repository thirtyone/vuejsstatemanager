import Vuex from 'vuex';
import Vue from 'vue';
import CartService from '../services/cart/CartService';
import CheckoutService from '../services/checkout/CheckoutService';
import CustomerService from '../services/customer/CustomerService';


Vue.use(Vuex);

export default new Vuex.Store({
    modules : {
        CartService,
        CheckoutService,
        CustomerService
    }
})